﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data.Common;

class Program
{
    public int Auswahl { get; set; }

    static void Main(string[] args)
    {
        Program program = new Program();
        program.logic();
        //CreateCommand("Update [MySchool].[dbo].[Course] Set Credits=Credits+1", "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=MySchool;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        //ReadOrderData("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=MySchool;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
    }

    private static void CreateCommand(string queryString,
    string connectionString)
    {
        using (SqlConnection connection = new SqlConnection(
                   connectionString))
        {
            SqlCommand command = new SqlCommand(queryString, connection);
            command.Connection.Open();
            command.ExecuteNonQuery();
        }
    }

    private static void ReadOrderData(string connectionString, string name)
    {
        string queryString = "SELECT * FROM dbo.Bills JOIN dbo.Customer ON dbo.Bills.CustomerID=dbo.Customer.CustomerID WHERE Customer.Name='" + name + "'";
        using (SqlConnection connection = new SqlConnection(
                   connectionString))
        {
            SqlCommand command = new SqlCommand(
                queryString, connection);
            connection.Open();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Console.WriteLine(String.Format("{0},{1},{2},{3},{4}",
                        reader[0], reader[1], reader[2], reader[3], reader[4]));
                }
            }
        }
    }

    private static string GetCustomerId(string connectionString, string name)
    {
        string queryString = "SELECT CustomerID FROM dbo.Customer WHERE Name='"+name+"'";
        using (SqlConnection connection = new SqlConnection(
                   connectionString))
        {
            SqlCommand command = new SqlCommand(
                queryString, connection);
            connection.Open();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                return reader[0].ToString();     

            }
        }
    }

    private static int GetLatestBillId(string connectionString)
    {
        string queryString = "SELECT TOP(1) BillID FROM dbo.Bills ORDER BY BillID DESC";
        using (SqlConnection connection = new SqlConnection(
                   connectionString))
        {
            SqlCommand command = new SqlCommand(
                queryString, connection);
            connection.Open();
            using (SqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                return (int)reader[0];

            }
        }
    }

    public void logic()
    {
        bool end = false;
        bool gueltigeEingabe = false;
        string eingabe;

        while (!end)
        {
            Console.WriteLine("Hello, you can:");
            Console.WriteLine("1. Retrieve all bills for a customer.");
            Console.WriteLine("2. Add a new bill.");
            Console.WriteLine("3. End the program.");

            while (!gueltigeEingabe)
            {
                eingabe = Console.ReadLine();
                if (Int32.TryParse(eingabe, out int auswahl))
                {
                    if (auswahl > 0 && auswahl < 4)
                    {
                        Auswahl = auswahl;
                        gueltigeEingabe = true;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("The entry wasn't correct. Please try again.");
                        Console.WriteLine("");
                        Console.WriteLine("You can:");
                        Console.WriteLine("1. Retrieve all bills for a customer.");
                        Console.WriteLine("2. Add a new bill.");
                        Console.WriteLine("3. End the program.");
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("The entry wasn't correct. Please try again.");
                    Console.WriteLine("");
                    Console.WriteLine("You can:");
                    Console.WriteLine("1. Retrieve all bills for a customer.");
                    Console.WriteLine("2. Add a new bill.");
                    Console.WriteLine("3. End the program.");
                }
            }
            gueltigeEingabe = false;

            switch (Auswahl)
            {
                case 1:
                    ReadBills();
                    break;
                case 2:
                    AddNewBill();
                    break;
                case 3:
                    Console.WriteLine("Thank you for using GreenTech Software!");
                    end = true;
                    break;
            }
        }

    }

    public void ReadBills()
    {
        Console.Clear();
        string name;
        Console.WriteLine("Please enter the name of the person you would like to see the bills from.");
        name = Console.ReadLine();
        ReadOrderData("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ScooTeq;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False", name);
    }

    public void AddNewBill()
    {
        string name;
        int minutes;
        bool gueltigeEingabe = false;
        int finalSum;
        string customerId;
        int latestBillId;
        int newBillId;
        string newBillIdString;
        Console.WriteLine("Please enter the name of the person with the new Bill.");
        name = Console.ReadLine();
        while (!gueltigeEingabe)
        {
            Console.WriteLine("Please enter the duration the person rode the scooter in minutes.");
            if (Int32.TryParse(Console.ReadLine(), out minutes))
            {
                Auswahl = minutes;
                gueltigeEingabe = true;
            }
            else
            {
                Console.Clear();
                Console.WriteLine("The entry wasn't correct. Please try again.");

            }
        }
        finalSum = Auswahl * 20 + 100;
        latestBillId = GetLatestBillId("Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = ScooTeq; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
        newBillId = latestBillId + 1;
        newBillIdString = newBillId.ToString();
        customerId = GetCustomerId("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ScooTeq;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False",name);
        CreateCommand("INSERT INTO Bills (BillID, CustomerID, Price) VALUES ("+newBillIdString+","+customerId+","+finalSum+")", "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = ScooTeq; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
    }



    //public void AddNewBill()
    //{
    //    bool gueltigeEingabe = false;
    //    string eingabe;

    //    Console.WriteLine("You can:");
    //    Console.WriteLine("1. Calculate a new fare");
    //    Console.WriteLine("2. Enter a set bill");

    //    while (!gueltigeEingabe)
    //    {
    //        eingabe = Console.ReadLine();
    //        if (Int32.TryParse(eingabe, out int auswahl))
    //        {
    //            if (auswahl > 0 && auswahl < 3)
    //            {
    //                Auswahl = auswahl;
    //                gueltigeEingabe = true;
    //            }
    //            else
    //            {
    //                Console.Clear();
    //                Console.WriteLine("Die Eingabe war leider ungueltig. Bitte versuchen sie es erneut!");
    //                Console.WriteLine("");
    //                Console.WriteLine("You can:");
    //                Console.WriteLine("1. Calculate a new fare");
    //                Console.WriteLine("2. Enter a set bill");
    //            }
    //        }
    //        else
    //        {
    //            Console.Clear();
    //            Console.WriteLine("Die Eingabe war leider ungueltig. Bitte versuchen sie es erneut!");
    //            Console.WriteLine("");
    //            Console.WriteLine("You can:");
    //            Console.WriteLine("1. Calculate a new fare");
    //            Console.WriteLine("2. Enter a set amount");
    //        }
    //    }
    //    if (Auswahl == 1)
    //    {
    //        //Fahrtpreis berechnen
    //    }
    //    else
    //    {
    //        Console.WriteLine("Please enter the amount of money in the bill.");
    //        while (!gueltigeEingabe)
    //        {

    //            eingabe = Console.ReadLine();
    //            if (Int32.TryParse(eingabe, out int auswahl))
    //            {
    //                Auswahl = auswahl;
    //                gueltigeEingabe = true;
    //            }
    //            else
    //            {
    //                Console.Clear();
    //                Console.WriteLine("Die Eingabe war leider ungueltig. Bitte versuchen sie es erneut!");
    //                Console.WriteLine("");
    //            }
    //        }
    //        gueltigeEingabe = false;
    //        //hier dann Auswahl oder beliebige andere Property einfügen in die Query
    //        CreateCommand("Update [MySchool].[dbo].[Course] Set Credits=Credits+1", "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = ScooTeq; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
    //    }
    //    gueltigeEingabe = false;
    //}

}